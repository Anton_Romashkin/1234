
<div class="ty-control-group ty-release_date-item cm-hidden-wrapper{if !$product.product_code} hidden{/if} cm-reload-{$obj_prefix}{$obj_id}" id="release_date_update_{$obj_prefix}{$obj_id}">
            <input type="hidden" name="appearance[show_release_date]" value="{$show_release_date}" />
            {if $show_sku_label}
                <label class="ty-control-group__label" id="release_date_{$obj_prefix}{$obj_id}">{__("first_addon.release_date")}:</label>
            {/if}
            <span class="ty-control-group__item">{$product.release_date}</span>